using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public Camera camera1;
    public Camera camera2;
    public Camera camera3;
    private int currentCameraIndex = 0;

    void Start()
    {
        // Désactiver toutes les caméras sauf la première
        camera1.enabled = true;
        camera2.enabled = false;
        camera3.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            SwitchCamera();
        }
    }

    void SwitchCamera()
    {
        // Désactiver la caméra actuelle
        if (currentCameraIndex == 0)
        {
            camera1.enabled = false;
            camera2.enabled = true;
            currentCameraIndex = 1;
        }
        else if (currentCameraIndex == 1)
        {
            camera2.enabled = false;
            camera3.enabled = true;
            currentCameraIndex = 2;
        }
        else if (currentCameraIndex == 2)
        {
            camera3.enabled = false;
            camera1.enabled = true;
            currentCameraIndex = 0;
        }
    }
}
